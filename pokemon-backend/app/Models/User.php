<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'nickname'
    ];

    public $timestamps = false;

    public function pokemon()
    {
        return $this->hasMany(Pokemon::class, 'user_id');
    }
}
