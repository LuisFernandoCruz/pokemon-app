<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use GuzzleHttp;

class PokemonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        $client = new GuzzleHttp\Client(array( 'curl' => array( CURLOPT_SSL_VERIFYPEER => false, ), ));
        $response = $client->request('GET', 'https://pokeapi.co/api/v2/pokemon/'.$this->number_pokede);
        $json = json_decode($response->getBody());
        return [
            'id' => $this->id,
            'number_pokede' => $this->number_pokede,
            'user_id' => $this->id,
            'name' => $json->name
        ];
    }
}
