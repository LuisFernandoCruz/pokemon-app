<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Models\{User, Pokemon};
use App\Http\Resources\{UserCollection, UserResource};

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $entrenadores = User::get();
        return response()->json([
            'status' => true,
            'entrenadores' => new UserCollection($entrenadores)
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\UserRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
        if(!isset($request->pokemon) || count($request->pokemon) != 6 ){
            return response()->json([
                'status' => false,
                'class' => 'error',
                'message' => 'No se selecciono a los 6 pokémon'
            ], 200);
        }
        $entrenador = new User;
        $entrenador->name = $request->nickname;
        $entrenador->save();

        foreach ($request->pokemon as $key => $p) {
            $newPokemon = new Pokemon;
            $newPokemon->number_pokede = $p;
            $newPokemon->user_id = $entrenador->id;
            $newPokemon->save();
        }

        return response()->json([
            'status' => true,
            'entrenador' => new UserResource($entrenador)
        ], 200);
    }
}
