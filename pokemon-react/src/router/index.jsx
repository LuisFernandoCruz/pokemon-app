import { Routes, BrowserRouter, Route } from "react-router-dom";
import Entrenadores from "../components/entrenadores/Entrenadores";
import Home from "../components/home/Home";

const Router = () => {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/entrenadores" element={<Entrenadores />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
