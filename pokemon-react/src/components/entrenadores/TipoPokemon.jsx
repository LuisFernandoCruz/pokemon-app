import { useState, useEffect } from "react";
import ApiClient from "../../config/ApiClient";

const TipoPokemon = ({ id }) => {
  const [tipo, setTipo] = useState({});

  useEffect(() => {
    const url = `https://pokeapi.co/api/v2/pokemon/${id}`;
    const getInfoPokemon = async () => {
      await ApiClient.get(url).then((response) => setTipo(response.data));
    };
    getInfoPokemon();
  }, []);

  return (
    tipo.types &&
    tipo.types.map((item) => <p className="text-sm">{item.type.name}</p>)
  );
};

export default TipoPokemon;
