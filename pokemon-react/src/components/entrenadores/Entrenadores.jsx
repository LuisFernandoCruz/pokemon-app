import { useState, useEffect } from "react";
import ApiClient from "../../config/ApiClient";
import DataTableHelper from "../helpers/DataTableHelper";
import Button from "react-bootstrap/Button";
import Modal from "react-bootstrap/Modal";
import TipoPokemon from "./TipoPokemon";

const Entrenadores = () => {
  const [entrenadores, setEntrenadores] = useState([]);
  const [pending, setPending] = useState(false);
  const [entrenador, setEntrenador] = useState({});

  useEffect(() => {
    setPending(true);
    const getEntrenadores = async () => {
      await ApiClient.get("/pokemon")
        .then((response) => {
          const responseEntrenadores = response.data.entrenadores;
          setEntrenadores(responseEntrenadores);
          setPending(false);
        })
        .catch((error) => console.log(error))
        .finally(() => setPending(false));
    };
    getEntrenadores();
  }, []);

  const columns = [
    {
      id: "id",
      name: "ID Usuario.",
      selector: (row) => row.id,
      sortable: true,
      center: true,
    },
    {
      id: "name",
      name: "Nickname usuario",
      selector: (row) => row.name,
      sortable: true,
      wrap: true,
    },
    {
      id: "pokemon[0]",
      name: "Pokémon 1",
      selector: (row) => row.pokemon[0].name,
      sortable: true,
      wrap: true,
    },
    {
      id: "pokemon[1]",
      name: "Pokémon 2",
      selector: (row) => row.pokemon[1].name,
      sortable: true,
      wrap: true,
    },
    {
      id: "pokemon[2]",
      name: "Pokémon 3",
      selector: (row) => row.pokemon[1].name,
      sortable: true,
      wrap: true,
    },
    {
      id: "pokemon[3]",
      name: "Detalle del equipo",
      button: true,
      selector: (row) => (
        <button className="btn btn-link" onClick={() => setEntrenador(row)}>
          {row.pokemon[3].name}
        </button>
      ),
    },
  ];

  const handleClose = () => setEntrenador({});

  return (
    <div>
      <DataTableHelper
        columns={columns}
        items={entrenadores}
        pending={pending}
      />
      <Modal show={entrenador.id} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Entrenador&nbsp;{entrenador.name}</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <div className="row">
            {entrenador.pokemon &&
              entrenador.pokemon.map((item) => (
                <div className="col-12 col-md-6 p-2" key={item.id}>
                  <div className="card">
                    <div className="card-body text-center">
                      <h5 className="card-title">
                        {item.name.charAt(0).toUpperCase() + item.name.slice(1)}
                      </h5>
                      <img
                        src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${item.id}.png`}
                        alt=""
                      />
                      <br />
                      <b>Tipo:</b> <TipoPokemon id={item.id} />
                    </div>
                  </div>
                </div>
              ))}
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Cerrar
          </Button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default Entrenadores;
