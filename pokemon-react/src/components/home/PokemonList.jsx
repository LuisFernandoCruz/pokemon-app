import { useState, useEffect } from "react";
import axios from "axios";

const PokemonList = (props) => {
  const [pokemonArray, setPokemonArray] = useState([]);
  const [page, setPage] = useState(0);
  const [perPage, setPerPage] = useState(18);
  const [count, setCount] = useState(0);
  const [reloadList, setReloadList] = useState(false);
  const [pending, setPending] = useState(false);
  const { pokemonSelected, setPokemonSelected } = props;

  useEffect(() => {
    setPending(true);
    const getPokemonList = async () => {
      await axios
        .get(
          `https://pokeapi.co/api/v2/pokemon?offset=${page}&limit=${perPage}`
        )
        .then((response) => {
          const results = response.data.results;
          const insertId = results.map((item) => {
            let id = item.url.split("/");
            item["id"] = id[6];
            return item;
          });
          setPokemonArray(insertId);
          setCount(response.data.count);
          setPending(false);
        })
        .catch((error) => {
          console.log(error);
          setPending(false);
        });
    };
    getPokemonList();
  }, [reloadList]);

  const previous = () => {
    if (page != 0 && page >= perPage) {
      setPage(page - perPage);
      setReloadList(!reloadList);
    }
  };

  const next = () => {
    if (parseInt(page + perPage) < count) {
      setPage(page + perPage);
      setReloadList(!reloadList);
    }
  };

  const Pagination = () => {
    return (
      <nav aria-label="Page navigation example">
        <ul className="pagination justify-content-center">
          <li
            className={`page-item ${
              page != 0 && page >= perPage ? "" : "disabled"
            }`}
          >
            <button className="page-link" onClick={previous}>
              Anterior
            </button>
          </li>

          <li
            className={`page-item ${
              parseInt(page + perPage) < count ? "" : "disabled"
            }`}
          >
            <button className="page-link" onClick={next}>
              Siguiente
            </button>
          </li>
        </ul>
      </nav>
    );
  };

  const selectPokemon = (id) => {
    if (pokemonSelected.includes(id)) {
      setPokemonSelected(pokemonSelected.filter((item) => item != id));
    } else {
      if (pokemonSelected.length < 6)
        setPokemonSelected([...pokemonSelected, id]);
    }
  };

  const style = (id) => {
    if (!pokemonSelected.includes(id) && pokemonSelected.length == 6) {
      return { cursor: "no-drop" };
    } else {
      return { cursor: "pointer" };
    }
  };

  return !pending ? (
    pokemonArray.length > 0 ? (
      <div>
        <div className="row p-2">
          {pokemonArray.map((item) => (
            <div
              className="col-4 col-md-2 p-2"
              key={item.id}
              onClick={() => selectPokemon(item.id)}
              style={style(item.id)}
            >
              <div
                className={`card ${
                  pokemonSelected.includes(item.id) ? "border-primary" : ""
                }`}
              >
                <div className="card-body text-center">
                  <h5 className="card-title">
                    {item.name.charAt(0).toUpperCase() + item.name.slice(1)}
                  </h5>
                  <img
                    src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${item.id}.png`}
                    alt=""
                  />
                </div>
              </div>
            </div>
          ))}
        </div>
        <Pagination />
      </div>
    ) : (
      <div className="text-center">Sin resultados...</div>
    )
  ) : (
    <div className="text-center">Cargando...</div>
  );
};

export default PokemonList;
