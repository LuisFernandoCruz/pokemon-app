import { useState } from "react";
import NicknameForm from "./NicknameForm";
import PokemonList from "./PokemonList";

const Home = () => {
  const [nickname, setNickname] = useState("");
  const [pokemonSelected, setPokemonSelected] = useState([]);

  return (
    <div>
      <NicknameForm
        nickname={nickname}
        setNickname={setNickname}
        pokemonSelected={pokemonSelected}
        setPokemonSelected={setPokemonSelected}
      />
      <PokemonList
        pokemonSelected={pokemonSelected}
        setPokemonSelected={setPokemonSelected}
      />
    </div>
  );
};
export default Home;
