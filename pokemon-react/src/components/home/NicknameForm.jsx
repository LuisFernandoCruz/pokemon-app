import { useState } from "react";
import ApiClient from "../../config/ApiClient";

const NicknameForm = (props) => {
  const { nickname, setNickname, pokemonSelected, setPokemonSelected } = props;
  const [errors, setErrors] = useState({});
  const [pending, setPending] = useState(false);

  const handlerOnChange = (target) => {
    if (target.name == "nickname") {
      if (target.value.length != 0 && target.value.length != 200) {
        setErrors({});
      }

      if (target.value.length == 0) {
        setErrors({
          nickname: ["El campo Nickname es obligatorio."],
        });
      }

      if (target.value.length > 200) {
        setErrors({
          nickname: ["El campo Nickname debe ser menor a 200 caracteres."],
        });
        return;
      }
      setNickname(target.value);
    }
  };

  const registrar = async () => {
    setPending(true);
    await ApiClient.post("/pokemon", {
      nickname: nickname,
      pokemon: pokemonSelected,
    })
      .then((response) => {
        if (response.data.status) {
          setNickname("");
          setPokemonSelected([]);
        }
        setPending(false);
      })
      .catch((error) => {
        setErrors(error.response.data.errors);
        setPending(false);
      });
  };

  return (
    <div className="d-flex flex-column justify-content-center align-items-center">
      <div className="p-2 d-flex">
        <label className="p-2">Nickname:</label>
        {!pending ? (
          <input
            className="form-control"
            value={nickname}
            name="nickname"
            onChange={(e) => handlerOnChange(e.target)}
          />
        ) : (
          <input
            className="form-control"
            value={nickname}
            name="nickname"
            disabled
          />
        )}
      </div>
      <div className="p-2">
        {errors.nickname && (
          <div className="form-text text-danger">{errors.nickname[0]}</div>
        )}
      </div>
      <div className="p-2">
        {!pending ? (
          <button
            type="button"
            className="btn btn-outline-primary"
            onClick={registrar}
          >
            Registrar
          </button>
        ) : (
          <button type="button" className="btn btn-outline-primary" disabled>
            Registrar
          </button>
        )}
      </div>
    </div>
  );
};

export default NicknameForm;
