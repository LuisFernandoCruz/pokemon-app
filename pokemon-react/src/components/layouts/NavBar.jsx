import Container from "react-bootstrap/Container";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";

const NavBar = () => {
  return (
    <Navbar bg="primary" variant="dark">
      <Container>
        <Navbar.Brand href="/">Pokémon App</Navbar.Brand>
        <Nav className="me-auto">
          <Nav.Link href="/">Pokémon checklist</Nav.Link>
          <Nav.Link href="/entrenadores">Entrenadores</Nav.Link>
        </Nav>
      </Container>
    </Navbar>
  );
};

export default NavBar;
