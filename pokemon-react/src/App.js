import { Fragment } from 'react';
import Router from './router'
import NavBar from './components/layouts/NavBar';

const App = () => {
  return (
    <Fragment>
      <NavBar />
      <Router />
    </Fragment>
  );
}

export default App;
